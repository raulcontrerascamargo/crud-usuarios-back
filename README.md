
# Proyecto de NestJS con PostgreSQL

Este proyecto de NestJS utiliza PostgreSQL como base de datos. A continuación se muestra la información básica de configuración:

## Configuración de la base de datos

- Puerto de conexión: 5432
- Nombre de usuario: postgres
- Contraseña: admin
- Base de datos: prueba

## Requisitos previos

Antes de ejecutar el proyecto, asegúrate de tener los siguientes requisitos previos instalados:

- Node.js
- npm (Administrador de paquetes de Node.js)
- PostgreSQL

## Configuración del proyecto

1. Clona el repositorio:

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
