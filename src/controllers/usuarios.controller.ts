import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import { Usuario } from '../entities/usuario.entity';
import { UsuariosService } from 'src/services/usuarios.service';
import { UsuarioGateway } from 'src/gateways/usuario/usuario.gateway';

@Controller('api/usuarios')
export class UsuariosController {
  constructor(
    private readonly usuarioService: UsuariosService,
    private readonly usuarioWebSocket: UsuarioGateway,
  ) {}

  @Get(':idUsuario')
  getUsuario(@Param('idUsuario') idUsuario: number): Promise<Usuario> {
    console.log('id usuario', idUsuario);
    return this.usuarioService.findById(idUsuario);
  }

  @Get('')
  getListadoUsuarios(): Promise<Usuario[]> {
    return this.usuarioService.findAll();
  }

  @Post('')
  crearUsuario(@Body() usuario: Usuario): Promise<Usuario> {
    return this.usuarioService.create(usuario);
  }

  @Put(':idUsuario')
  actualizarUsuario(
    @Param('idUsuario') idUsuario: number,
    @Body() body: any,
  ): Promise<Usuario> {
    return this.usuarioService.actualizarUsuario(idUsuario, body);
  }

  @Delete(':idUsuario')
  eliminarUsuario(@Param('idUsuario') idUsuario: string): Promise<string> {
    this.usuarioService
      .remove(idUsuario)
      .then(() => this.usuarioWebSocket.notificarUsuarioEliminado(idUsuario));

    return;
  }
}
