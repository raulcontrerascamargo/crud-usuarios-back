import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class UsuarioGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  handleDisconnect(client: any) {
    console.log('usuario desconectado');
  }
  handleConnection(client: any, ...args: any[]) {
    console.log('usuario conectado');
  }
  afterInit(server: any) {
    console.log('iniciando websocket');
  }
  @WebSocketServer()
  server: Server;

  notificarUsuarioEliminado(idUsuario: string) {
    this.server.emit('userDeleted', idUsuario);
  }
}
