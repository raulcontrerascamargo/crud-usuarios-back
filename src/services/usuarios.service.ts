import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from '../entities/usuario.entity';

@Injectable()
export class UsuariosService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}

  async findById(id: number) {
    const object = await this.usuarioRepository.findOne({ where: { id } });
    return object;
  }

  async findAll(): Promise<Usuario[]> {
    return this.usuarioRepository.find();
  }

  async create(user: Usuario): Promise<Usuario> {
    return this.usuarioRepository.save(user);
  }

  async remove(id: string): Promise<void> {
    await this.usuarioRepository.delete(id);
  }

  async actualizarUsuario(id: number, actUsuario: Usuario) {
    const usuario = await this.usuarioRepository.findOne({ where: { id } });

    usuario.nombre = actUsuario.nombre;
    usuario.apellido = actUsuario.apellido;
    usuario.edad = actUsuario.edad;
    usuario.telefono = actUsuario.telefono;
    usuario.correo = actUsuario.correo;

    await this.usuarioRepository.save(usuario);

    return usuario;
  }
}
