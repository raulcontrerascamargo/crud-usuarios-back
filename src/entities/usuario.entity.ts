import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'usuarios' })
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  nombre: string;

  @Column()
  apellido: string;

  @Column()
  edad: number;

  @Column()
  telefono: string;

  @Column()
  correo: string;
}
